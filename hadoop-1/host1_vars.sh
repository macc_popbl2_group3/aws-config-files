#!/bin/bash -ex
export NODE1="10.0.10.12"
export NODE2="10.0.10.13"
export NODE3="10.0.10.14"
export NODE_ID="1"

echo "[*] Change default SSH port"
if [ $(cat /etc/ssh/sshd_config | grep Port | wc -l) -ne 0 ]; then
  sudo sed -i -e '1,/#Port [a-zA-Z0-9]*/s/#Port [a-zA-Z0-9]*/Port 4444/' /etc/ssh/sshd_config
  sudo sed -i -e '1,/Port [a-zA-Z0-9]*/s/Port [a-zA-Z0-9]*/Port 4444/' /etc/ssh/sshd_config
fi

echo "[*] Applying the changes..."
sudo systemctl restart sshd
echo "[*] SSH hardened!"

sudo sed -i 's/^\(10\.0\.10\.12.*\)$/10.0.10.12 master\n\1/g' /etc/hosts
sudo echo "10.0.10.12 master" >> /etc/hosts
sudo echo "10.0.10.13 slave1" >> /etc/hosts
sudo echo "10.0.10.14 slave2" >> /etc/hosts

cd /tmp

sudo docker pull adeiarias/hadoop-hbase
sudo docker run -itd --name master --hostname master --network host adeiarias/hadoop-hbase

sudo docker exec master service ssh start
sudo docker exec master gosu hadoop:hadoop bash -c '/usr/share/hadoop/sbin/start-all.sh'
sudo docker exec master gosu hadoop:hadoop bash -c '/usr/share/hbase/bin/start-hbase.sh'
sudo docker exec master gosu hadoop:hadoop bash -c 'hbase thrift start &'
sudo docker exec master gosu hadoop:hadoop bash -c '/usr/share/spark/sbin/start-all.sh'
sudo docker exec master gosu hadoop:hadoop hdfs dfs -mkdir /batch_data
sudo docker exec master gosu hadoop:hadoop hdfs dfs -mkdir /nifi_provenance
sudo docker exec master gosu hadoop:hadoop hdfs dfs -mkdir /spark-logs
sudo docker exec master gosu hadoop:hadoop hdfs dfs -mkdir /models
sudo docker exec master gosu hadoop:hadoop hdfs dfs -chown nifi:supergroup /batch_data
sudo docker exec master gosu hadoop:hadoop hdfs dfs -chmod 777 /batch_data
sudo docker exec master gosu hadoop:hadoop hdfs dfs -chown nifi:supergroup /nifi_provenance
sudo docker exec master gosu hadoop:hadoop hdfs dfs -chmod 777 /nifi_provenance
sudo docker exec master apt update
sudo docker exec master apt install python3-pip -y -qq
sudo docker exec master apt install nano -y

sudo docker exec master gosu hadoop:hadoop tail -f /dev/null

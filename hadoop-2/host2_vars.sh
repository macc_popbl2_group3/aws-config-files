#!/bin/bash -ex
export NODE1="10.0.10.12"
export NODE2="10.0.10.13"
export NODE3="10.0.10.14"
export NODE_ID="2"

echo "[*] Change default SSH port"
if [ $(cat /etc/ssh/sshd_config | grep Port | wc -l) -ne 0 ]; then
  sudo sed -i -e '1,/#Port [a-zA-Z0-9]*/s/#Port [a-zA-Z0-9]*/Port 4444/' /etc/ssh/sshd_config
  sudo sed -i -e '1,/Port [a-zA-Z0-9]*/s/Port [a-zA-Z0-9]*/Port 4444/' /etc/ssh/sshd_config
fi

echo "[*] Applying the changes..."
sudo systemctl restart sshd
echo "[*] SSH hardened!"

sudo sed -i 's/^\(10\.0\.10\.13.*\)$/10.0.10.13 slave1\n\1/g' /etc/hosts
sudo echo "10.0.10.12 master" >> /etc/hosts
sudo echo "10.0.10.13 slave1" >> /etc/hosts
sudo echo "10.0.10.14 slave2" >> /etc/hosts

cd /tmp

sudo docker pull adeiarias/hadoop-hbase
sudo docker run -itd --name slave1 --hostname slave1 --network host adeiarias/hadoop-hbase

sudo docker exec slave1 sh -c "service ssh start; sleep 15"
sudo docker exec slave1 hadoop-daemon.sh start datanode
sudo docker exec slave1 gosu hadoop:hadoop /usr/share/spark/sbin/start-worker.sh spark://master:7077
sudo docker exec slave1 gosu hadoop:hadoop tail -f /dev/null
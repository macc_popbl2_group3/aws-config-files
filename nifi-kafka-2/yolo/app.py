import uvicorn
import pandas as pd
import base64
import json
from fastapi import FastAPI
from ultralytics import YOLO
from pydantic import BaseModel

# Clase que hereda de BaseModel. Representa la imagen que se va a observar
# Es la estructura de los datos que se van a mandar a la IA para la predicción
# Tiene que heredar de esta clase para que FastAPI entienda el string de Base64 como body y no como parámtero de la URL
class Image(BaseModel):
  fecha: str
  borough: str
  image: str

# Crear la app
app = FastAPI()

@app.get('/')
def index():

  '''
  Enpoint del index, funciona como health check para comprobar que la API responde
  '''

  return  {
            'message' : 'Welcome to MUseguro'
          }


@app.post('/detect')
def detect_objects(image: Image):
    
    '''
    Endpoint para hacer la detección de objetos \n
    Recibe mediante POST un string de base64 que representa la imagen que se quiere observar \n
    Devuelve un json indicando cuántas instancias de cada objeto ha detectado
    '''
    
    # Decodificar string de base64 a imagen
    image_data = base64.b64decode(image.image)
    image_name = "img_to_observe.jpg"
    with open(image_name, 'wb') as f:
        f.write(image_data)

    # Carga del modelo
    print("Cargando modelo...")
    model_name = 'yolobikecrashes.pt' # Para evitar fallos, que el nombre no sea 'yolov8n.pt' ni ninguno definido ya por ultralytics!!!
    model = YOLO(model_name)
    
    # Detección de la imagen
    print("Observando imagen...")
    results = model.predict(source=image_name, save=False, imgsz=640)
    print("Devolviendo los objetos detectados...")
    
    # Transormar los datos de YOLO a un json del conteo de objetos
    data = transform_predict_to_json(results, model.names)

    # print(res)
    return data

def transform_predict_to_json(results: list, labels_dict: dict) -> str:
    
    # Pasar el tensor a numpy array
    predict_bbox = results[0].to("cpu").numpy().boxes.xyxy
    
    # Ir rellenando la lista para contar cuánto hay de cada
    class_counts = {
    'bicycle': 0, 
    'bus': 0, 
    'car': 0, 
    'cng': 0, 
    'motorcycle': 0, 
    'other-vehicle': 0, 
    'person': 0, 
    'rickshaw': 0, 
    }
    
    for i in range(len(predict_bbox)):
        class_name = labels_dict[int(results[0].to("cpu").numpy().boxes.cls[i])]
        
        class_counts[class_name] += 1
    
    # output_json = [{'name': class_name, 'count': count} for class_name, count in class_counts.items()]
    return class_counts

# Inicia la app en el host y puerto especificados (Se puede cambiar si queremos!!!)
if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8001)
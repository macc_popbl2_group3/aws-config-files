echo "Creating keyspace..."
cqlsh -e "CREATE KEYSPACE IF NOT EXISTS weather WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};"

cqlsh -e "CREATE TABLE IF NOT EXISTS weather.measurements (uuid TEXT PRIMARY KEY, feelslike_c TEXT, uv TEXT, last_updated TEXT, wind_degree TEXT, wind_dir TEXT, gust_mph TEXT, temp_c TEXT, precip_mm TEXT, cloud TEXT, wind_kph TEXT, condition TEXT, vis_km TEXT, humidity TEXT, pressure_mb TEXT, sunrise TEXT, sunset TEXT);"

echo "Keyspace created."
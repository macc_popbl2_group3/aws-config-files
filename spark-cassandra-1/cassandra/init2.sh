#!/bin/bash

set -e

# Esperar hasta que el servicio de Cassandra esté disponible
until cqlsh -e "DESCRIBE KEYSPACES" > /dev/null 2>&1; do
    sleep 1
    echo "waiting..."
done

echo "Creating keyspace..."

# Crear el keyspace si no existe
cqlsh -e "CREATE KEYSPACE IF NOT EXISTS weather WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};"
cqlsh -e "CREATE KEYSPACE IF NOT EXISTS crashes WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};"
cqlsh -e "CREATE KEYSPACE IF NOT EXISTS camera WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};"
cqlsh -e "CREATE KEYSPACE IF NOT EXISTS hdfsimages WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};"

echo "Keyspace created."

echo "Creating table..."

# Crear la tabla si no existe
cqlsh -e "CREATE TABLE IF NOT EXISTS weather.measurements (uuid TEXT PRIMARY KEY, feelslike_c TEXT, uv TEXT, last_updated TEXT, wind_degree TEXT, wind_dir TEXT, gust_mph TEXT, temp_c TEXT, precip_mm TEXT, cloud TEXT, wind_kph TEXT, condition TEXT, vis_km TEXT, humidity TEXT, pressure_mb TEXT, sunrise TEXT, sunset TEXT);"
cqlsh -e "CREATE TABLE IF NOT EXISTS crashes.crash_data (crash_date TEXT, crash_time TEXT, borough TEXT, zip_code TEXT, latitude TEXT, longitude TEXT, cross_street_name TEXT, number_of_persons_injured TEXT, number_of_persons_killed TEXT, number_of_pedestrians_injured TEXT, number_of_pedestrians_killed TEXT, number_of_cyclist_injured TEXT, number_of_cyclist_killed TEXT, number_of_motorist_injured TEXT, number_of_motorist_killed TEXT, contributing_factor_vehicle_1 TEXT, contributing_factor_vehicle_2 TEXT, contributing_factor_vehicle_3 TEXT, contributing_factor_vehicle_4 TEXT, contributing_factor_vehicle_5 TEXT, collision_id TEXT PRIMARY KEY, vehicle_type_code1 TEXT, vehicle_type_code2 TEXT, vehicle_type_code_3 TEXT, vehicle_type_code_4 TEXT, vehicle_type_code_5 TEXT, cars TEXT, people TEXT);"
cqlsh -e "CREATE TABLE IF NOT EXISTS camera.detections (uuid TEXT PRIMARY KEY, borough TEXT, date TEXT, cars TEXT, bikes TEXT, people TEXT);"
cqlsh -e "CREATE TABLE IF NOT EXISTS hdfsimages.metadata (fecha TEXT, filename TEXT PRIMARY KEY, borough TEXT);"

#Crear secondary indexes
cqlsh -e "CREATE INDEX IF NOT EXISTS last_updated_index ON weather.measurements (last_updated);"
cqlsh -e "CREATE INDEX IF NOT EXISTS crash_date_index ON crashes.crash_data (crash_date);"
cqlsh -e "CREATE INDEX IF NOT EXISTS date_index ON camera.detections (date);"
cqlsh -e "CREATE INDEX IF NOT EXISTS fecha_index ON hdfsimages.metadata (fecha);"

echo "Table created."

echo "Initialization completed."

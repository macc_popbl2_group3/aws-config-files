from functools import partial
from pyspark.sql.functions import rand, when, to_timestamp, date_format, col
from pyspark.sql import SparkSession
from pyspark.sql.functions import from_json, expr, lit
from pyspark.sql.types import StructType, StructField, StringType, DoubleType, IntegerType, FloatType
import json

spark = SparkSession.builder.appName("TrainPredict").getOrCreate()
spark.sparkContext.setLogLevel("WARN")

from pyspark.ml.classification import DecisionTreeClassificationModel

from pyspark.ml.feature import StringIndexer
from pyspark.ml.feature import VectorAssembler

model = DecisionTreeClassificationModel.load("model_path")

#METER DATOS EN CAMARA----------------------------------------------------------------------------------------------------
schema = StructType([
    StructField("feelslike_c", FloatType(), nullable=False),
    StructField("uv", FloatType(), nullable=False),
    StructField("last_updated", StringType(), nullable=False),
    StructField("sunrise", StringType(), nullable=False),
    StructField("wind_degree", IntegerType(), nullable=False),
    StructField("bicycle", IntegerType(), nullable=False),
    StructField("borough", StringType(), nullable=False),
    StructField("wind_dir", StringType(), nullable=False),
    StructField("gust_mph", FloatType(), nullable=False),
    StructField("temp_c", FloatType(), nullable=False),
    StructField("precip_mm", FloatType(), nullable=False),
    StructField("cloud", IntegerType(), nullable=False),
    StructField("fecha", StringType(), nullable=False),
    StructField("wind_kph", FloatType(), nullable=False),
    StructField("condition", StringType(), nullable=False),
    StructField("car", IntegerType(), nullable=False),
    StructField("person", IntegerType(), nullable=False),
    StructField("sunset", StringType(), nullable=False),
    StructField("vis_km", FloatType(), nullable=False),
    StructField("humidity", IntegerType(), nullable=False),
    StructField("pressure_mb", FloatType(), nullable=False),
])

# data = [
#     (19.4, 6.0, "2021-09-11 00:00", "05:25 AM", 249, 0, "Bronx", "WSW", 7.8, 19.4, 0.0, 100, "2023-06-09 18:24", 3.6, "Overcast", 1, 1, "08:26 PM", 16.0, 49, 1008.0)
# ]

# dfPrediccion = spark.createDataFrame(data, schema)
#--------------------------------------------------------------------------------------------------------------------------

#OIHANA------------------------------------------------------------------------------

json_data=""

data_for_prediction = []

for dato in json_data:
    data_for_prediction.append(dato)

dfPrediccion = spark.createDataFrame(data_for_prediction, schema)

#--------------------------------------------------------------------------------------------------------------------------

indexers = [StringIndexer(inputCol=column, outputCol=column+"_encoded").fit(dfPrediccion) for column in ['borough', 'condition',
                                                                                                 'last_updated', 'sunrise', 
                                                                                                 'sunset', 'wind_dir', 'fecha']]
data_indexed = dfPrediccion
for indexer in indexers:
    data_indexed = indexer.transform(data_indexed)

assembler = VectorAssembler(
    inputCols=['borough_encoded', 'condition_encoded', 'last_updated_encoded', 'sunrise_encoded', 'sunset_encoded', 'wind_dir_encoded', 
               'fecha_encoded', 'feelslike_c', 'uv', 'wind_degree', 'gust_mph', 'temp_c', 'precip_mm', 'cloud', 'wind_kph', 'humidity', 
               'pressure_mb', 'bicycle', 'car', 'person', 'vis_km'],
    outputCol='features'
)

data_assembled = assembler.transform(data_indexed)

predictions = model.transform(data_assembled)
predictions.select('prediction').show()
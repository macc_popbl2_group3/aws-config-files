from functools import partial
from pyspark.sql.functions import rand, when, to_timestamp, date_format, col
from pyspark.sql import SparkSession
from pyspark.sql.functions import from_json, expr, lit
from pyspark.sql.types import StructType, StructField, StringType, DoubleType, IntegerType, FloatType
import json

spark = SparkSession.builder.appName("TrainModel").getOrCreate()
spark.sparkContext.setLogLevel("WARN")

spark = SparkSession.builder.appName("Spark Cassandra Example").config("spark.cassandra.connection.host", "spark-cassandra-1_cassandra-seed_1").config("spark.cassandra.connection.port", "9042").config("spark.cassandra.auth.username", "cassandra").config("spark.cassandra.auth.password", "cassandra").getOrCreate()
dfAccidentes = spark.read.format("org.apache.spark.sql.cassandra").options(table="crash_data", keyspace="crashes").load()
dfTiempo = spark.read.format("org.apache.spark.sql.cassandra").options(table="measurements", keyspace="weather").load()
dfCamara = spark.read.format("org.apache.spark.sql.cassandra").options(table="detections", keyspace="camera").load()

print(dfAccidentes)

dfAccidentes = dfAccidentes.withColumn("accidente", lit(1))

print(dfTiempo)

print(dfCamara)
dfCamara.show(5)

dfCamara = dfCamara.withColumnRenamed("uuid", "uuid2")
dfCamara = dfCamara.withColumnRenamed("borough", "borough2")
dfCamara = dfCamara.withColumnRenamed("cars", "cars2")
dfCamara = dfCamara.withColumnRenamed("people", "people2")

dfAccidentes = dfAccidentes.withColumn("crash_date", date_format(to_timestamp("crash_date", "yyyy-MM-dd'T'HH:mm:ss.SSS"), "yyyy-MM-dd HH:mm"))
dfTiempo = dfTiempo.withColumn("last_updated", date_format(to_timestamp("last_updated", "yyyy-MM-dd HH:mm"), "yyyy-MM-dd HH:mm"))
dfCamara = dfCamara.withColumn("date", date_format(to_timestamp("date", "yyyy-MM-dd HH:mm"), "yyyy-MM-dd HH:mm"))

resultado = dfAccidentes.join(dfTiempo, dfAccidentes["crash_date"] == dfTiempo["last_updated"], "outer")
resultado2 = resultado.join(dfCamara, (resultado["crash_date"] == dfCamara["date"]) & (resultado["borough"] == dfCamara["borough2"]), "outer")

print(resultado2)

resultado2 = resultado2.na.fill({"accidente": 0})

resultado2.select("accidente").show(5)

resultado2 =  resultado2.na.fill(value="0")
resultado2 = resultado2.fillna("0")
resultado2 = resultado2.na.replace('', '0')

resultado2 = resultado2.drop("cars")
resultado2 = resultado2.drop("zip_code")
resultado2 = resultado2.drop("people")
resultado2 = resultado2.drop("collision_id")
resultado2 = resultado2.drop("uuid")
resultado2 = resultado2.drop("uuid2")
resultado2 = resultado2.drop("contributing_factor_vehicle_1")
resultado2 = resultado2.drop("contributing_factor_vehicle_2")
resultado2 = resultado2.drop("contributing_factor_vehicle_3")
resultado2 = resultado2.drop("contributing_factor_vehicle_4")
resultado2 = resultado2.drop("contributing_factor_vehicle_5")
resultado2 = resultado2.drop("crash_date")
resultado2 = resultado2.drop("crash_time")
resultado2 = resultado2.drop("cross_street_name")
resultado2 = resultado2.drop("latitude")
resultado2 = resultado2.drop("longitude")
resultado2 = resultado2.drop("number_of_cyclist_injured")
resultado2 = resultado2.drop("number_of_cyclist_killed")
resultado2 = resultado2.drop("number_of_motorist_injured")
resultado2 = resultado2.drop("number_of_motorist_killed")
resultado2 = resultado2.drop("number_of_pedestrians_injured")
resultado2 = resultado2.drop("number_of_pedestrians_killed")
resultado2 = resultado2.drop("number_of_persons_injured")
resultado2 = resultado2.drop("number_of_persons_killed")
resultado2 = resultado2.drop("vehicle_type_code1")
resultado2 = resultado2.drop("vehicle_type_code2")
resultado2 = resultado2.drop("vehicle_type_code_3")
resultado2 = resultado2.drop("vehicle_type_code_4")
resultado2 = resultado2.drop("vehicle_type_code_5")
resultado2 = resultado2.drop("number_of_persons_killed")
resultado2 = resultado2.drop("number_of_persons_killed")
resultado2 = resultado2.drop("borough2")

resultado2 = resultado2.withColumnRenamed("bikes", "bicycle")
resultado2 = resultado2.withColumnRenamed("cars2", "car")
resultado2 = resultado2.withColumnRenamed("people2", "person")
resultado2 = resultado2.withColumnRenamed("date", "fecha")

resultado2 = resultado2.withColumn("feelslike_c", col("feelslike_c").cast("float"))
resultado2 = resultado2.withColumn("uv", col("uv").cast("float"))
resultado2 = resultado2.withColumn("wind_degree", col("wind_degree").cast("integer"))
resultado2 = resultado2.withColumn("temp_c", col("temp_c").cast("float"))
resultado2 = resultado2.withColumn("precip_mm", col("precip_mm").cast("float"))
resultado2 = resultado2.withColumn("cloud", col("cloud").cast("integer"))
resultado2 = resultado2.withColumn("wind_kph", col("wind_kph").cast("float"))
resultado2 = resultado2.withColumn("humidity", col("humidity").cast("integer"))
resultado2 = resultado2.withColumn("pressure_mb", col("pressure_mb").cast("float"))
resultado2 = resultado2.withColumn("gust_mph", col("gust_mph").cast("float"))
resultado2 = resultado2.withColumn("vis_km", col("vis_km").cast("float"))
resultado2 = resultado2.withColumn("bicycle", col("bicycle").cast("integer"))
resultado2 = resultado2.withColumn("car", col("car").cast("integer"))
resultado2 = resultado2.withColumn("person", col("person").cast("integer"))
resultado2 = resultado2.withColumn("accidente", col("accidente").cast("integer"))

resultado2.printSchema()

resultado2 =  resultado2.na.fill(value="0")
resultado2 = resultado2.fillna("0")
resultado2 = resultado2.na.replace('', '0')

cols = resultado2.columns

from pyspark.ml import Pipeline
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.feature import HashingTF, Tokenizer

from pyspark.ml.feature import OneHotEncoder, StringIndexer, VectorAssembler

indexers = [StringIndexer(inputCol=column, outputCol=column+"_indexed").fit(resultado2) for column in ['borough', 'condition',
                                                                                                 'last_updated', 'sunrise', 
                                                                                                 'sunset', 'wind_dir', 'fecha']]
data_indexed = resultado2
for indexer in indexers:
    data_indexed = indexer.transform(data_indexed)

encoders = [OneHotEncoder(inputCol=column+"_indexed", outputCol=column+"_encoded") for column in ['borough', 'condition',
                                                                                                 'last_updated', 'sunrise', 
                                                                                                 'sunset', 'wind_dir', 'fecha']]
encoder_models = [encoder.fit(data_indexed) for encoder in encoders]
data_encoded = data_indexed
for encoder_model in encoder_models:
    data_encoded = encoder_model.transform(data_encoded)

assembler = VectorAssembler(
    inputCols=['borough_encoded', 'condition_encoded', 'last_updated_encoded', 'sunrise_encoded', 'sunset_encoded', 'wind_dir_encoded', 
               'fecha_encoded', 'feelslike_c', 'uv', 'wind_degree', 'gust_mph', 'temp_c', 'precip_mm', 'cloud', 'wind_kph', 'humidity', 
               'pressure_mb', 'bicycle', 'car', 'person', 'vis_km'],
    outputCol='features'
)

data_assembled = assembler.transform(data_encoded)

data_assembled = data_assembled.withColumnRenamed('accidente', 'label')

train_data, test_data = data_assembled.randomSplit([0.7, 0.3], seed=42)

from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.evaluation import BinaryClassificationEvaluator

dt = DecisionTreeClassifier(labelCol='label', featuresCol='features')

dt_model = dt.fit(train_data)

predictions = dt_model.transform(test_data)

evaluator = BinaryClassificationEvaluator(labelCol='label')
accuracy = evaluator.evaluate(predictions)
print(f"Accuracy: {accuracy}")
dt_model.write().overwrite().save('model_path')